(function () {

// объект змейка
var snake = function () {
	var that = {},
		cells = [], // ячейки змейки
		context,
		fieldSize,
		cellSize,
		snakeElement,
		bodyColor,
		errorBodyColor;

	// начальное заполнение змейки ячейками и отрисовка
	that.initSnake = function ( property ) {
		context = property.context;
		fieldSize = property.fieldSize;
		cellSize = property.cellSize;	
		bodyColor = property.bodyColor;
		errorBodyColor = property.errorBodyColor;

		for (var i = 0; i < property.length; i++) {
			cells[i] = [fieldSize / 2 + i, fieldSize / 2];
			drawRect(cells[i][0], cells[i][1]);
		}
		makeSnakeElement();
	}

	// движение змейки
	that.step = function ( where, keyValues ) {
		var result = 0; // результат для таймера
		clearSnake(); // очистка старой змейки

		// конец игры
		var endGame = function () {
			context.beginPath();
			context.rect(1, 1, fieldSize * cellSize - 2, fieldSize * cellSize - 2);
			context.fillStyle = errorBodyColor;
			context.fill();
			context.stroke();			
		}

		// рисование красной змейки, для ошибочных ходов
		var redDraw = function () {				
			for (var i = 0; i < cells.length; i++) {
				drawRect(cells[i][0], cells[i][1], errorBodyColor);
			}
			result = 1;
		}

		// создание новой ячейки змейки
		var newCell = function (nCell) {

			// проверка не столкнулась ли змейка с границей
			if (nCell[0] < 0 || nCell[1] < 0 || nCell[0] + 1 > fieldSize || nCell[1] + 1 > fieldSize) {
				endGame();
				result = 2;
				return;
			} 

			// проверка не столкнулась ли змейка с собой
			for (var i = 0; i < cells.length; i++) {
				if (nCell[0] == cells[i][0] && nCell[1] == cells[i][1]) { 
					endGame();
					result = 3; 
					return;
				}
			}

			cells.unshift(nCell); 
			if (cells[0][0] == snakeElement[0] && cells[0][1] == snakeElement[1]) {
				makeSnakeElement();
				result = 4; // змейка выросла
			}
			else cells.splice(-1, 1); 

			for (var i = 0; i < cells.length; i++) {
				drawRect(cells[i][0], cells[i][1]);
			}
		}

		// проверка по направлению движения, как должен измениться массив ячеек
		switch (where) {
			case keyValues.topValue: //top

				if (cells[0][1] - 1 == cells[1][1]) redDraw();
				else newCell([cells[0][0], cells[0][1] - 1]);

				break;
			case keyValues.rightValue: //right

				if (cells[0][0] + 1 == cells[1][0]) redDraw();
				else newCell([cells[0][0] + 1, cells[0][1]]);

				break;
			case keyValues.bottomValue: //bottom

				if (cells[0][1] + 1 == cells[1][1]) redDraw();
				else newCell([cells[0][0], cells[0][1] + 1]);

				break;
			case keyValues.leftValue: //left  

				if (cells[0][0] - 1 == cells[1][0]) redDraw();
				else newCell([cells[0][0] - 1, cells[0][1]]);

				break;
		}

		return result;
	}

	// создание куска змейки для роста
	var makeSnakeElement = function () {
		var randX = Math.floor( Math.random() * fieldSize ),
			randxY = Math.floor( Math.random() * fieldSize );
		snakeElement = [randX, randxY];
		drawRect(randX, randxY);
	}

	// рисование прямоугольника
	var drawRect = function (x, y, color) {
		context.beginPath();
		context.rect(x * cellSize + 2, y * cellSize + 2, cellSize - 4, cellSize - 4);
		if (color) context.fillStyle = errorBodyColor;
		else context.fillStyle = bodyColor;
		context.fill();
		context.stroke();
	}

	// очистка змейки
	var clearSnake = function () {
		console.log("!");
		for (i = 0; i < cells.length; i++) {
			context.clearRect((cells[i][0]) * cellSize + 1, (cells[i][1]) * cellSize + 1, cellSize - 2, cellSize - 2);
		}
	}


	var autoPassing = function () {

	}

	return that;
}

// объект контроллера
var controller = function () {
	var that = {},
		fieldProperty = {
			"fieldSize" : 50, // размер поля
			"cellSize" : 10, // размер ячейки
			"borderWidth" : 1,
			"borderColor" : "#b4b4b4",
		},
		timer,
		keyValues = {
			"leftValue" : 37,
			"topValue" : 38,
			"rightValue" : 39,
			"bottomValue" : 40
		},
		stepKey = keyValues.leftValue,  // направление движения
		tmpStepKey = keyValues.leftValue, 
		stepCount = 0,
		speed = 180,
		timeStepValue = {
			"noWay" : 1,
			"fieldEnd" : 2,
			"eatingMyself" : 3,
			"grow" : 4,
		},
		timeStep,
		speedReduction = {
			"lineValue" : 20,
			"beforLineReduction" : 10,
			"afterLineReduction" : 1
		},
		canvas = document.getElementById("snake"),
		context = canvas.getContext("2d"),
		mySnake = snake(), // змейка
		snakeProperty = {
			"length" : 10, // начальная длина змейки
			"context" : context,
			"fieldSize" : fieldProperty.fieldSize,
			"cellSize" : fieldProperty.cellSize,
			"bodyColor" : "black",
			"errorBodyColor" : "red",
		},
		messages = {
			"endEatingYourself" : "Змейка съела сама себя :( ",
			"endEndOfField" : "Змейка погибла из-за выхода за пределы своего мира :( ",
		};

	// метод инициализации
	that.init = function () {

		drawLines(); // рисование сетки 

		mySnake.initSnake(snakeProperty); 

		// навешивание обработчика на нажатие клавиши. Учитывались только браузеры поддерживающие навешивание событий по W3C
		window.addEventListener("keyup", function(event) {	
			if (event.keyCode == keyValues.leftValue || event.keyCode == keyValues.topValue || event.keyCode == keyValues.rightValue || event.keyCode == keyValues.bottomValue) stepKey = event.keyCode;
		});

		// таймер
		timer = setInterval(function() {
			timeStep = mySnake.step(stepKey, keyValues);
			if (timeStep == timeStepValue.noWay) stepKey = tmpStepKey; // если змейка пошла не туда, то откатываем к предыдущему направлению
			else tmpStepKey = stepKey; // резервируем правильное направление на случай ошибки на следующем шаге
			
			// если змейка вернула, что она увидела приделы поля
			if (timeStep == timeStepValue.fieldEnd) {
				alert(messages.endEndOfField);
				clearInterval(timer);
			}
			// змейка вернула, что она напоролась на саму себя
			else if (timeStep == timeStepValue.eatingMyself) {
				alert(messages.endEatingYourself);
				clearInterval(timer);
			}
			// змейка вернула, что она увеличилась, увеличиваем скорость
			else if (timeStep == timeStepValue.grow) {
				if (speed != speedReduction.lineValue) speed -= speedReduction.beforLineReduction;
				else if (speed <= speedReduction.lineValue && speed != speedReduction.afterLineReduction) speed -= speedReduction.afterLineReduction;
			}

		}, speed);

	}

	// функция рисования сетки 
	var drawLines = function () {

		canvas.width = fieldProperty.cellSize * fieldProperty.fieldSize;
		canvas.height = fieldProperty.cellSize * fieldProperty.fieldSize;

		// draw cells
		for (var i = 0; i < fieldProperty.fieldSize + 1; i++) {
	        context.lineWidth = fieldProperty.borderWidth;
	        context.strokeStyle = fieldProperty.borderColor; 

			context.beginPath();
	        context.moveTo(i * fieldProperty.cellSize, 0);
	        context.lineTo(i * fieldProperty.cellSize, fieldProperty.cellSize * fieldProperty.fieldSize);
	        context.stroke();

			context.beginPath();
	        context.moveTo(0, i * fieldProperty.cellSize);
	        context.lineTo(fieldProperty.cellSize * fieldProperty.fieldSize, i * fieldProperty.cellSize);
	        context.stroke();
		}
	}

	return that;
};

$(window).load(function() {
	var cont = controller();
	cont.init();
});

}());	